# 个人网站1

#### 介绍
使用gitee pages服务，创建自己的网站。我的网址：[传送门](https://ikaros-521.gitee.io/self.gitee.io)

# 相关教程视频
b站[传送门](https://www.bilibili.com/video/av64294697) ，博客 [传送门](https://blog.csdn.net/Ikaros_521/article/details/99580361)

# 总体效果

![主界面](https://images.gitee.com/uploads/images/2019/0823/091744_471a3067_5140590.png "屏幕截图.png")

# 相关功能介绍

左侧的一连串图片添加了click事件，使用jQuery库函数，实现显示和隐藏效果。

右上角“康纳”是“投食模块”

右下角50px到150px范围隐藏了“导航栏页面”的超链接

右下的看板娘 需要的相关文件打包在“2d”中，还需要jQuery以及html的相关添加和调用

游戏模块：同理在码云其他仓库开启服务，进行超链接跳转